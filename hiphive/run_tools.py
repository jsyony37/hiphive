#!/usr/bin/python
import numpy as np
import scipy as sp
import os

from argparse import ArgumentParser
from configparser import ConfigParser
from copy import copy, deepcopy
from monty.serialization import dumpfn, loadfn
from joblib import Parallel, delayed
from time import time
import psutil
import matplotlib.pyplot as plt

from ase.io.vasp import read_vasp, read_vasp_out, write_vasp
from ase import Atoms
from ase.cell import Cell

import scipy
from scipy.constants import elementary_charge as q

from pymatgen.core.structure import Structure
from pymatgen.io.ase import AseAtomsAdaptor

from phonopy import Phonopy
from phonopy.structure.atoms import PhonopyAtoms
from phono3py.phonon3.gruneisen import Gruneisen

from hiphive import ForceConstantPotential, ClusterSpace, StructureContainer, ForceConstants, enforce_rotational_sum_rules
from hiphive.force_constant_model import ForceConstantModel
from hiphive.utilities import get_displacements
from hiphive.fitting import Optimizer
from hiphive.input_output.logging_tools import logger

from joblib import Parallel, delayed
import time

logger = logger.getChild(__name__)

hbar = sp.constants.hbar
kB = sp.constants.Boltzmann
q = sp.constants.elementary_charge

def read_input_file():
    """ Reads hiphive input file """
    parser = ArgumentParser()
    parser.add_argument("--file", "-f", help="settings file. Default input_hiphive", default='input_hiphive')
    parser.add_argument("--renormalize", action='store_true', help="Anharmonic temperature renormalization of harmonic FC.", default=False)
    options = parser.parse_args()    
    inputs = ConfigParser()
    if not os.path.exists(options.file):
        logger.info("ERROR: settings file {} not found".format(options.file))
        exit(-1)
    inputs.read(options.file)
    
    return options, inputs


def read_structures(i,supercell,training_dir,temp=None):
    """ Reads POSCAR and OUTCAR of atom-displaced supercells
    i: int
      Displacement index
    supercell: Atoms
      Original undisplaced supercell
    training_dir: str
      The directory where files are located
    temp: float
       If given, thermalized structures are at the given temp are used for fitting
       If None, then randomly displaced structures are used   
    """
    if temp is None:
        disp = training_dir+str(i/1000)
    else:
        disp = 'thermalized-'+str(temp)+'K/disp-'+str(i)
    structure = read_vasp(disp+'/POSCAR')
    logger.info('Read {}'.format(disp))
    displacements = get_displacements(structure,supercell)
    structure.new_array('displacements', displacements)
    structure.positions = supercell.positions
    vaspout = read_vasp_out(disp+'/OUTCAR')
    force = vaspout.get_forces()
    structure.new_array('forces', force)
    logger.info('         Max, Avg. displacement {}, {}'.format(abs(displacements).max(),abs(displacements).mean()))
    logger.info('         Max, Avg. force {}, {}'.format(abs(force).max(),abs(force).mean()))
    return structure


def get_fit_data(cs,supercell,training_dir,minmax,temp=None,param2=None):
    """ Constructs structure container of atom-displaced supercells
    cs: ClusterSpace 
    supercell: Atoms
      Original undisplaced supercell
    training_dir: str
      The directory where files are located  
    minmax: numpy.array
      Minimum and maximum displacement indices
    temp: float
      Temperature at which thermalized structures are generated 
    param2: numpy.array
      Harmonic force constant parameters, triggers sequential fitting
    """

    saved_structures = []
    fcm = ForceConstantModel(supercell,cs)
    for i in range(minmax[0],minmax[1]):
        if param2 is None: # fit harmonic or all
            try:
                structure = read_structures(i,supercell,training_dir,temp)
            except:
                continue
        elif param2 is not None: # fit anharmonic separately
            try:
                structure = read_structures(i,supercell,training_dir,temp)
            except:
                continue
        saved_structures.append(structure)
    natom = supercell.get_global_number_of_atoms()
    ndim = natom*3
    nrow_all = ndim*len(saved_structures)
    A_mat = np.zeros((nrow_all,cs.n_dofs)) # set up fit matrix
    f_vec = np.zeros(nrow_all)
    fit_data_tmp = Parallel(n_jobs=min(os.cpu_count(),max(1,len(saved_structures))))(
        delayed(construct_fit_data)(fcm,structure,s)
        for s,structure in enumerate(saved_structures)
    )
    for s,data in enumerate(fit_data_tmp):
        A_mat[ndim*s:ndim*(s+1)] = data[0]
        f_vec[ndim*s:ndim*(s+1)] = data[1]

    if param2 is not None:
        f_vec -= np.dot(A_mat[:,:len(param2)],param2) # subtract harmonic force from total

    logger.info('Fit_data dimensions: {}'.format(A_mat.shape))
    fit_data = _clean_data(A_mat,f_vec,ndim)
    return fit_data

def construct_fit_data(fcm,structure,s):
    displacements = structure.get_array('displacements')
    forces = structure.get_array('forces')
    A = fcm.get_fit_matrix(displacements)
    f = forces.ravel()
    return [A,f]

def training(cs,supercell,training_dir,disp_minmax,fit_method,separate_fit,phonopy,mesh,temp=None):
    """ Performs force constant fitting
    cs: ClusterSpace
    supercell: Atoms
       Original undisplaced supercell   
    disp_minmax: numpy.array [harmonic,anharmonic or all]
       Minimum and maximum displacement indices
    fit_method: 
    temp: float
       If given, thermalized structures are at the given temp are used for fitting
       If None, then randomly displaced structures are used
    """
    ndim = supercell.get_global_number_of_atoms()*3
    n2nd = cs.get_n_dofs_by_order(2)
    nall = cs.n_dofs

    # Set up structure container - for harmonic fit
    logger.info('Fitting harmonic force constants only to check for imaginary modes...')
    fit_data = get_fit_data(cs,supercell,training_dir,disp_minmax[0],temp=None,param2=None)
    opt = Optimizer(fit_data,fit_method,[0,n2nd])#,standardize=False) 
    opt.train()
    logger.info(opt)
    param2 = opt.parameters
    param_tmp = np.concatenate((param2,np.zeros(cs.n_dofs-len(param2))))
    fcp = ForceConstantPotential(cs, param_tmp)
    fcs = fcp.get_force_constants(supercell)
    phonopy.set_force_constants(fcs.get_fc_array(order=2))
    phonopy.set_mesh(mesh, is_eigenvectors=False, is_mesh_symmetry=False,is_gamma_center=True)
    phonopy.run_mesh(mesh, with_eigenvectors=False, is_mesh_symmetry=False,is_gamma_center=True)
    omega = phonopy.mesh.frequencies  # THz
    omega = np.sort(omega.flatten())
    logger.info('Frequencies (THz) from harmonic-only fitting: \n {}'.format(omega[:12]))
    imaginary = np.any(omega<-1e-3)
   
    if imaginary and separate_fit:#False:
        logger.info('Imaginary modes found! Fitting anharmonic force constants separately...')
        fit_data = get_fit_data(cs,supercell,training_dir,disp_minmax[1],temp,param2=param2)
        opt = Optimizer(fit_data,fit_method,[len(param2),nall])#,standardize=False)
        opt.train()
        logger.info(opt)
        param_anh = opt.parameters
        param = np.concatenate((param2,param_anh))
        assert cs.n_dofs == len(param)
    else:
        logger.info('Fitting all orders together...')
        fit_data = get_fit_data(cs,supercell,training_dir,[np.min(disp_minmax),np.max(disp_minmax)],temp=None,param2=None)
        opt = Optimizer(fit_data,fit_method,[0,nall])#,standardize=False)
        opt.train()
        logger.info(opt)
        param = opt.parameters
                
    param = enforce_rotational_sum_rules(cs, param, ["Huang", "Born-Huang"])
    logger.info('##### Parameters #####')
    logger.info(param.shape)
    logger.info(param)

    # Construct and save force constant potential
    np.savetxt('parameters',param)
    fcp = ForceConstantPotential(cs, param)
    fcp.write('solution.fcp')
    fcs = fcp.get_force_constants(supercell)

    return fcs, param


def _clean_data(A,f,ndim):
    """ Removes linearly dependent force equations
        Three in each supercell corresponding to one atom
    """
    ind = list(np.array([[i*ndim-3,i*ndim-2,i*ndim-1] for i in range(1,1+int(A.shape[0]/ndim))]).flatten())
    A = np.delete(A,ind,0)
    f = np.delete(f,ind,0)
    logger.info('{} after cleaning'.format(A.shape))
    return([A,f])


def save_fcs(fcs,prim,supercell,anh_save,harmonic_only=False,temp=None):
    """ Save FC to Phonopy/ShengBTE & Hierarchical formats
    """
    fc2_name = 'FORCE_CONSTANTS_2ND'
    fc2hdf_name = 'fc2'
    if temp is not None:
        fc2_name = fc2_name+'_'+str(temp)+'K'
        fc2hdf_name = fc2hdf_name+'_'+str(temp)+'K'
    fcs.write_to_phonopy(fc2_name ,'text')
    with open(fc2_name, 'r') as f:
        lines = f.readlines()
        lines[0] = str(lines[0].split()[0])+' \n'
    with open(fc2_name, 'w') as f:
        f.writelines(lines)
    logger.info('{} written!'.format(fc2_name))
    fcs.write_to_phonopy(fc2hdf_name+'.hdf5')
    logger.info('{} written!'.format(fc2hdf_name))
    if not harmonic_only:
        for order in anh_save:
            if order==3:
                fcs.write_to_phono3py('fc3.hdf5', order=3)
                logger.info('fc3.hdf5 written!')
                fcs3 = ForceConstants.read_phono3py(supercell, 'fc3.hdf5', order=3)
                fcs3.write_to_shengBTE('FORCE_CONSTANTS_3RD', prim, order)
                logger.info('FORCE_CONSTANTS_3RD written!')
            elif order==4:
                fcs.write_to_phono3py('fc4.hdf5', order=4)
                logger.info('fc4.hdf5 written!')
                fcs4 = ForceConstants.read_phono3py(supercell, 'fc4.hdf5', order=4)
                fcs4.write_to_shengBTE('FORCE_CONSTANTS_4TH', prim, order)
                logger.info('FORCE_CONSTANTS_4TH written!')
            else:
                logger.warning('Only order = 3 or 4 accepted! Not writing {}th-order FC.'.format(order))


def harmonic_properties(fcs,phonopy,mesh,temp):
    """ Calculate harmonic properties: dispersion, free energy, entropy, heat capacity
    fcs: ForceConstants
    phonopy: Phonopy
    mesh: List
         list of integers for mesh in x,y,z
    temp: List
         list of temperatures at which to calculate thermal properties
    """
    phonopy.set_force_constants(fcs.get_fc_array(order=2))
    phonopy.set_mesh(mesh, is_eigenvectors=True, is_mesh_symmetry=False,is_gamma_center=True)
    phonopy.set_thermal_displacements(temperatures=temp)
    phonopy.run_thermal_properties(temperatures=temp) # calculate harmonic properties
    omega = phonopy.mesh.frequencies  # THz
    evec = phonopy.mesh.eigenvectors
    logger.info('Frequency in THz')
    logger.info(np.sort(omega.flatten()))
    if np.any(omega<-1e-2):
        logger.info('Imaginary modes found!')
    else:
        logger.info('No imaginary modes!')
    _, msds = phonopy.get_thermal_displacements()
    msds = np.sum(msds, axis=1)  # sum of the MSD over x,y,z
    for temperature, msd in zip(temp, msds):
        logger.info('T = {} K    MSD = {} A**2'.format(temperature,msd))
    _, free_energy, entropy, Cv = phonopy.get_thermal_properties()
    natom = phonopy.primitive.get_number_of_atoms()
    free_energy *= 1000/sp.constants.Avogadro/natom # kJ/mol to eV/atom
    Cv *= 1/sp.constants.Avogadro/natom # J/K/mol to eV/K/atom
    entropy *= 1/sp.constants.Avogadro/natom # J/K/mol to eV/K/atom

    return omega, evec, free_energy, entropy, Cv


def plot_dispersion(fcs,name,fit_method,primitive_fname,scmat,temp,TD=True):
    if TD:
        os.system('sumo-phonon-bandplot -e -f FORCE_CONSTANTS_2ND_'+str(temp[0])+'K -p '+name+'-'+str(fit_method)+'_'+str(temp[0])+'K --poscar '+str(primitive_fname)\
                  +' --dim '+str(scmat[0,0])+' '+str(scmat[1,1])+' '+str(scmat[2,2])+' --pymatgen --unit meV --height 7 --width 10')
        os.system('mv sumo_band.yaml phonon_'+str(temp[0])+'K.yaml')    
    else:
        os.system('sumo-phonon-bandplot -e -f FORCE_CONSTANTS_2ND -p '+name+'-'+str(fit_method)+' --poscar '+str(primitive_fname)\
                  +' --dim '+str(scmat[0,0])+' '+str(scmat[1,1])+' '+str(scmat[2,2])+' --pymatgen --unit meV --height 7 --width 10')
        os.system('mv sumo_band.yaml phonon.yaml')

        
def free_energy_correction(omega_h,omega_TD,evec_h,evec_TD,temp):
    """ Makes two correction to QH free energy
    1. By enforcing S = -(dF/dT)_V  (Cowley, Allen)
    2. Explicit 4th order correction derived from perturbation theory
    """
    nqpt, nmode = omega_TD.shape
    omega_TD = omega_TD*1e12*2*np.pi + 1 # THz to radian Hz, avoid 0
    omega_h = omega_h*1e12*2*np.pi + 1  # THz to radian Hz, avoid 0
    omega_h0 = copy(omega_h)
    omega_h0[omega_h0 < 0] = 0 # imaginary frequencies to 0
    correction_S_t = np.zeros(len(temp))
    correction_SC_t = np.zeros(len(temp))
    for i, t in enumerate(temp):
        bose = np.array(1/(np.exp(hbar*abs(omega_TD)/(kB*(t+1e-3)))-1))
        correction_S = -hbar/2*np.sum((omega_TD-omega_h0)*(bose+1/2))/nqpt
        omega2_h_mod = np.zeros(omega_h.shape)
        for k in range(nqpt):
            omega2_h_matrix = np.eye(nmode)*np.sign(omega_h[k])*omega_h[k]**2
            unitary = np.matmul(np.linalg.inv(evec_h[k]),evec_TD[k])
            omega2_h_mod[k] = np.matmul(unitary.T,np.matmul(omega2_h_matrix,unitary)).diagonal()
        correction_SC = -hbar/4*np.sum((omega_TD**2*np.sign(omega_TD)-omega2_h_mod)/omega_TD*(bose+1/2))/nqpt
        correction_S *= 1/(nmode/3)/q # eV per atom 
        correction_SC *= 1/(nmode/3)/q # eV per atom 
        correction_S_t[i] = correction_S
        correction_SC_t[i] = correction_SC
    logger.info('Free Energy CORRECTION_S , CORRECTION_SC in eV/atom: \n {}, {}'.format(correction_S,correction_SC))
    if len(temp)==1:
        return correction_S, correction_SC
    else:
        return correction_S_t, correction_SC_t


def get_total_grun(omega,grun,kweight,T):
    total = 0
    weight = 0
    thresh = 0.05
    nptk, nbands = omega.shape
    omega = abs(omega)*1e12*2*np.pi
    if T==0:
        total = np.zeros((3,3))
        grun_total_diag = np.zeros(3)
    else:
        for i in range(nptk):
            for j in range(nbands):
                x = hbar*omega[i,j]/(2.0*kB*T)
                dBE = (x/(np.sinh(x)+1e-20))**2
                weight += dBE*kweight[i]
                total += dBE*kweight[i]*grun[i,j]
        total = total/weight
        grun_total_diag = np.array([total[0,2],total[1,1],total[2,0]])

        def percent_diff(a,b):
            return abs((a-b)/b)
        # This process preserves cell symmetry upon thermal expansion, i.e., it prevents
        # symmetry-identical directions from inadvertently expanding by different ratios
        # when the Gruneisen routine returns slighlty different ratios for those directions

        avg012 = np.mean((grun_total_diag[0],grun_total_diag[1],grun_total_diag[2]))
        avg01 = np.mean((grun_total_diag[0],grun_total_diag[1]))
        avg02 = np.mean((grun_total_diag[0],grun_total_diag[2]))
        avg12 = np.mean((grun_total_diag[1],grun_total_diag[2]))
        if percent_diff(grun_total_diag[0],avg012) < thresh:
            if percent_diff(grun_total_diag[1],avg012) < thresh:
                if percent_diff(grun_total_diag[2],avg012) < thresh: # all siilar
                    grun_total_diag[0] = avg012
                    grun_total_diag[1] = avg012
                    grun_total_diag[2] = avg012
                elif percent_diff(grun_total_diag[2],avg02) < thresh: # 0 and 2 similar
                    grun_total_diag[0] = avg02
                    grun_total_diag[2] = avg02
                elif percent_diff(grun_total_diag[2],avg12) < thresh: # 1 and 2 similar
                    grun_total_diag[1] = avg12
                    grun_total_diag[2] = avg12
                else:
                    pass
            elif percent_diff(grun_total_diag[1],avg01) < thresh: # 0 and 1 similar
                grun_total_diag[0] = avg01
                grun_total_diag[1] = avg01
            elif percent_diff(grun_total_diag[1],avg12) < thresh: # 1 and 2 similar
                grun_total_diag[1] = avg12
                grun_total_diag[2] = avg12
            else:
                pass
        elif percent_diff(grun_total_diag[0],avg01) < thresh: # 0 and 1 similar
            if percent_diff(grun_total_diag[0],avg02) < thresh or percent_diff(grun_total_diag[1],avg12) < thresh: # 0 or 1 and 2 also similar
                grun_total_diag[0] = avg012
                grun_total_diag[1] = avg012
                grun_total_diag[2] = avg012
            else: # only 0 and 1 similar
                grun_total_diag[0] = avg01
                grun_total_diag[1] = avg01
        elif percent_diff(grun_total_diag[0],avg02) < thresh: # 0 and 2 similar
            if percent_diff(grun_total_diag[0],avg01) < thresh or percent_diff(grun_total_diag[2],avg12) < thresh: # 0 or 2 and 1 also similar
                grun_total_diag[0] = avg012
                grun_total_diag[1] = avg012
                grun_total_diag[2] = avg012
            else: # only 0 and 2 similar
                grun_total_diag[0] = avg02
                grun_total_diag[2] = avg02
        elif percent_diff(grun_total_diag[1],avg12) < thresh: # 1 and 2 similar
            if percent_diff(grun_total_diag[1],avg01) < thresh or percent_diff(grun_total_diag[2],avg02) < thresh: # 1 or 2 and 0 also similar
                grun_total_diag[0] = avg012
                grun_total_diag[1] = avg012
                grun_total_diag[2] = avg012
            else: # only 1 and 2 similar
                grun_total_diag[1] = avg12
                grun_total_diag[2] = avg12                
        else: # nothing similar
            pass

    logger.info('TOTAL GRUNEISEN')
    logger.info(total)
    
    return grun_total_diag

def get_total_grun_old(omega,grun,kweight,T):
    total = 0
    weight = 0
    nptk, nbands = omega.shape
    omega = abs(omega)*1e12*2*np.pi
    if T==0:
        total = np.zeros((3,3))
        grun_total_diag = np.zeros(3)
    else:
        for i in range(nptk):
            for j in range(nbands):
                x = hbar*omega[i,j]/(2.0*kB*T)
                dBE = (x/np.sinh(x))**2
                weight += dBE*kweight[i]
                total += dBE*kweight[i]*grun[i,j]
        total = total/weight
        grun_total_diag = np.array([total[0,2],total[1,1],total[2,0]])

        def percent_diff(a,b):
            return abs((a-b)/b)
        # This process preserves cell symmetry upon thermal expansion, i.e., it prevents
        # symmetry-identical directions from inadvertently expanding by different ratios
        # when the Gruneisen routine returns slighlty different ratios for those directions
            
        if percent_diff(grun_total_diag[0],grun_total_diag[1]) < 0.1:
            avg = np.mean((grun_total_diag[0],grun_total_diag[1]))
            grun_total_diag[0] = avg
            grun_total_diag[1] = avg
        elif percent_diff(grun_total_diag[0],grun_total_diag[2]) < 0.1:
            avg = np.mean((grun_total_diag[0],grun_total_diag[2]))
            grun_total_diag[0] = avg
            grun_total_diag[2] = avg
        elif percent_diff(grun_total_diag[1],grun_total_diag[2]) < 0.1:
            avg = np.mean((grun_total_diag[1],grun_total_diag[2]))
            grun_total_diag[1] = avg
            grun_total_diag[2] = avg
        else:
            pass
    logger.info('TOTAL GRUNEISEN')
    logger.info(total)
    return grun_total_diag


def gruneisen(phonopy,fcs2,fcs3,mesh,temperature,Cv,bulk_mod):
    
    logger.info('##### Calculating Gruneisen and thermal expansion... #####')
    vol = phonopy.primitive.volume
    gruneisen = Gruneisen(fcs2,fcs3,phonopy.supercell,phonopy.primitive)
    gruneisen.set_sampling_mesh(mesh,is_gamma_center=True)
    gruneisen.run()
    grun = gruneisen.get_gruneisen_parameters() # (nptk,nmode,3,3)
    omega = gruneisen._frequencies
    qp = gruneisen._qpoints
    kweight = gruneisen._weights
    grun_tot = list()
    for temp in temperature:
        grun_tot.append(get_total_grun(omega,grun,kweight,temp))
    grun_tot = np.nan_to_num(np.array(grun_tot))
    # linear thermal expansion coefficient     
    cte = grun_tot*(Cv.repeat(3).reshape((len(Cv),3)))/(vol/10**30)/(bulk_mod*10**9)/3
    cte = np.nan_to_num(cte)    
    logger.info('Frequency : {}'.format(np.sort(omega.flatten())))
    logger.info('Gruneisen : {}'.format(grun_tot))
    logger.info('CTE : {}'.format(cte))    
    return grun_tot, cte


def expand_lattice(temperature,cte,T=None):
    print('Temperature array:',temperature)
    print('CTE array:',cte)
    assert len(temperature)==len(cte)
    if 0 not in temperature:
        temperature = [0] + temperature
        cte = np.insert(cte,0,cte[0]/2,axis=0)
    print('Temperature array:',temperature)
    print('CTE array:',cte)
    temperature = np.array(temperature)
    ind = np.argsort(temperature)
    temperature = temperature[ind]
    cte = np.array(cte)[ind]
    # linear expansion fraction
    dLfrac = copy(cte)    
    for t in range(len(temperature)):
        dLfrac[t,:] = np.trapz(cte[:t+1,:],temperature[:t+1],axis=0)
    dLfrac = np.nan_to_num(dLfrac)
    logger.info('dLfrac : {}'.format(dLfrac[-1]))
    if T is None:
        return dLfrac
    else:
        try:
            T_ind = np.where(temperature==T)[0][0]
            return np.array([dLfrac[T_ind]])
        except:
            raise ValueError('Designated T does not exist in the temperature array!')

    
def gruneisen_command(primitive_fname,symprec,dim,mesh,temperatures,Cv,bulk_mod,vol) :

    import subprocess
    import h5py

    logger.info('##### Gruneisen and Thermal Expansion #####')
    # Anharmonic properties by internal call to phono3py (Gruneisen function)
    phono3py_cmd = 'phono3py -c '+primitive_fname+' --tolerance='+str(symprec)+' --dim="'+str(dim[0])+' '+str(dim[1])+' '+str(dim[2])+\
                '" --fc2 --fc3 --sym-fc --br --gruneisen --mesh="'+str(mesh[0])+' '+str(mesh[1])+' '+str(mesh[2])+'" --ts="{0}"'.format(' '.join(str(T) for T in temperatures))
    logger.info(phono3py_cmd)
    subprocess.call(phono3py_cmd, shell=True)
    grun = np.array(h5py.File('gruneisen.hdf5','r')['gruneisen'])
    omega = np.array(h5py.File('gruneisen.hdf5','r')['frequency'])*1e12*2*np.pi
    kweight = np.array(h5py.File('gruneisen.hdf5','r')['weight'])
    grun_tot = list()
    for temp in temperatures:
        grun_tot.append(get_total_grun(omega,grun,kweight,temp))
    grun_tot = np.nan_to_num(grun_tot)
    intCv = np.zeros(len(Cv))
    for i, temp in enumerate(temperatures):
        if temp == 0:
            intCv[i] = 0
        elif len(temperatures) == 1:
            intCv[i] = np.trapz(np.array([0,Cv[i]]),np.array([0,temp]))
        else:
            intCv[i] = np.trapz(Cv[0:i+1],temperatures[0:i+1])  # integrate for cumulative Cv
    cte = Cv*grun_tot/(vol/10**30)/(bulk_mod*10**9)/3
    dLfrac = intCv*grun_tot/(vol/10**30)/(bulk_mod*10**9)/3
    cte = np.nan_to_num(cte)
    dLfrac = np.nan_to_num(dLfrac)
    logger.info('Gruneisen : {}'.format(grun_tot))
    logger.info('CTE : {}'.format(cte))
    logger.info('dLfrac : {}'.format(dLfrac))
    
    return grun_tot, cte, dLfrac


def save_thermal_data(temp,free_energy,entropy,Cv,grun_tot,cte,dLfrac,TD=True,correction_TI=None,
                      correction_S=None,correction_SC=None,correction_int4=None,correction_int6=None):
    free_energy /= q # in eV
    entropy /= q # in eV/K
    Cv /= q # in eV/K
    # CTE in /K
    # grun and dlfrac are dimensionless
    thermal_data = dict()
    if TD:
        keys = ['temperature','free_energy','correction_TI','correction_S','correction_SC',#'correction_int4','correction_int6',
                'entropy','heat_capacity','gruneisen','cte','dl']
        values = [temp,free_energy,correction_TI,correction_S,correction_SC,#correction_int4,correction_int6,
                  entropy,Cv,grun_tot,cte,dLfrac]
        for i, key in enumerate(keys):
            thermal_data[key] = values[i]
        dumpfn(thermal_data,'thermal_properties_{}K.json'.format(temp[0]))
    else:
        keys = ['temperature','free_energy','entropy','heat_capacity','gruneisen','cte','dl']
        values = [temp,free_energy,entropy,Cv,grun_tot,cte,dLfrac]
        for i, key in enumerate(keys):
            thermal_data[key] = values[i]
        dumpfn(thermal_data,'thermal_properties.json')
        

def setup_TE_renorm(prim,cs,supercell,dLfrac,scmat,param,cutoffs,T):
    prim_TE = prim.copy()
    cell_TE = Cell(np.transpose([prim_TE.get_cell()[:,i]*(1+dLfrac[0,i])  for i in range(3)]))
    prim_TE.set_cell(cell_TE,scale_atoms=True)
    supercell_TE = supercell.copy()
    cell_TE = Cell(np.transpose([supercell_TE.get_cell()[:,i]*(1+dLfrac[0,i])  for i in range(3)]))
    supercell_TE.set_cell(cell_TE,scale_atoms=True)
    count = 0
    failed = False
    cs_TE = ClusterSpace(prim_TE,cutoffs,symprec=1e-2,acoustic_sum_rules=True) # try original cutoff first - might work
    while True:
        count += 1
        if cs_TE.n_dofs == cs.n_dofs:
            break
        elif count>10:
            logger.warning("Could not find ClusterSpace for expanded cell identical to the original cluster space!")
            failed = True
            break
        elif count==1:
            cutoffs_TE = [i*(1+np.linalg.norm(dLfrac)) for i in cutoffs]
        elif cs_TE.n_dofs > cs.n_dofs:
            cutoffs_TE = [i*0.999 for i in cutoffs_TE]
        elif cs_TE.n_dofs < cs.n_dofs:
            cutoffs_TE = [i*1.001 for i in cutoffs_TE]
        cs_TE = ClusterSpace(prim_TE,cutoffs_TE,symprec=1e-2,acoustic_sum_rules=True)
    if failed:
        return None, None, None, None, None, failed
    else:
        fcp_TE = ForceConstantPotential(cs_TE,param)
        fcs_TE = fcp_TE.get_force_constants(supercell_TE)
        cs_TE.write('cluster_space_{}K.cs'.format(T))
        write_vasp('POSCAR_{}K'.format(T),prim_TE,direct=True)
        write_vasp('SPOSCAR_{}K'.format(T),supercell_TE,direct=True)
        prim_TE_phonopy = PhonopyAtoms(symbols=prim_TE.get_chemical_symbols(),
                                       scaled_positions=prim_TE.get_scaled_positions(), cell=prim_TE.cell)
        phonopy_TE = Phonopy(prim_TE_phonopy, supercell_matrix=scmat, primitive_matrix=None)
        return prim_TE, supercell_TE, cs_TE, fcs_TE, phonopy_TE, failed
