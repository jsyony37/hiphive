import scipy
import random
from numpy import *
import numpy as np
from numpy.linalg import *
from copy import copy, deepcopy

import time, psutil

from pymatgen.io.ase import AseAtomsAdaptor

from hiphive import StructureContainer, ForceConstantPotential, ClusterSpace, enforce_rotational_sum_rules
from hiphive.force_constant_model import ForceConstantModel
from hiphive.structure_container import FitStructure
from hiphive.fitting import Optimizer
from hiphive.utilities import get_displacements
from hiphive.input_output.logging_tools import logger

from hiphive.calculators import ForceConstantCalculator
from hiphive.run_tools import *

from joblib import Parallel, delayed

logger = logger.getChild(__name__)

kB = 0.086173 # Boltzmann's constant in meV/K
unit2thz = 98.1761/2/np.pi  # eV/A^2/au to THz
unit2mev = unit2thz*4.135665538536  # eV/A^2/au to meV

class Renormalization:
    """ Class for temperature-dependent anharmonic renormalization of
        harmonic force constants and phonons

    Parameters
    ----------
    cs : ClusterSpace
        The cluster space the model is based upon
    sc : StructureContainer
        For getting the sensing matrix (and force)
    supercell : Atoms
        Original supercell object
    parameters : numpy.ndarray
        The fitted paramteres (symmetry-distinct force constants)
    temp : float
        Temperature
    renorm_method : str
        Method of updating harmonic force constants. Must be one of
        "least_squares": default; uses the Moore-Penrose pseudoinverse of A2 to update harmonic FC 
        "refit": uses the fit_method to directly fit for harmonic FC
    fit_method: str
        Method of refitting 2nd order force constants if renorm_method=refit
    metadata : dict
        metadata dictionary, will be pickled when object is written to file
    """
    
    def __init__(self,cs,supercell,param,fcs,temp,renorm_method,fit_method=None,param_EH=None,fcs_EH=None,set_im_to=5,metadata=None):
        self.temp = temp
        self.param_BO = param     # original DFT-fitted IFC parameters, not updated
        self.fcs_BO = fcs         # original DFT-fitted IFCs, not updated  
        self.param_EH = param_EH  # if provided, it has been renormalized once
        self.fcs_EH = fcs_EH      # if provided, it has been renormalized once
        self.supercell = supercell
        self.cs = cs
        self.fcm = ForceConstantModel(self.supercell,self.cs)
        self.sc = StructureContainer(cs)
        self.renorm_method = renorm_method
        self.fit_method = fit_method
        self.set_im_to = set_im_to
        self._sanity_check()
        # add metadata
        if metadata is None:
            metadata = dict()
        self._metadata = metadata
        self._add_default_metadata()

    def _sanity_check(self):
        if self.cs.n_dofs != len(self.param_BO):
            raise ValueError("Number of DoF different from number of parameters")
        if self.fcs_BO.n_atoms != len(self.supercell):
            raise ValueError("Number of atoms in fcs and supercell are different")
    
    def thermalize_training_set(self,nconfig,use_seed):
        if self.cs.n_dofs != len(self.param_BO):
            raise ValueError("Number of DoF different from number of parameters")
        if self.fcs_BO.n_atoms != len(self.supercell):
            raise ValueError("Number of atoms in fcs and supercell are different")
        if self.renorm_method not in ['refit','least_squares']:
            raise ValueError("Invalid renorm_method! Must be one of: refit, least_squares")
        if self.renorm_method=='refit' and self.fit_method is None:
            raise ValueError('Refit scheme was opted abut fit_method was not provided.')

    def write_thermalize_configurations(self,nconfig,use_seed):
        """ This method generates random temperature-dependent 
            displaced (thermalized) supercell configurations

        Parameters
        ----------
        nconfig : int
             Number of configurations to generate
        use_seed : bool
             If True, use random seed to sample consistent atomic configurations

        Returns
        -------
        structures : StructureContainer
             Contains all displaced supercell configurations
        """

        logger.info('### Thermalizing structures at {} K ###'.format(str(self.temp)))
        assert(self.fcs_EH is not None)
        matcov, feng, imaginary = self.get_qcv(self.fcs_EH.get_fc_array(2))
        Lmatcov = self.cholesky_lower(matcov)
        structures = self.qcv_sample(Lmatcov,nconfig,0,use_seed)
        dir_name = 'thermalized_configs_{}K'.format(self.temp)
        os.mkdir(dir_name)
        for i,s in enumerate(structures):
            write_vasp('{}/POSCAR_{}K_config{}'.format(dir_name,self.temp,i),s)
                
        
    def renormalize(self,nconfig):#,conv_thresh=None):
        """ This method performs anharmonic phonon renormalization routine
            by generating thermalized supercell configurations and updating
            harmonic force constants in an interative manner until vibrational
            free energy converges to a desired threshold.

        Parameters
        ----------
        nconfig : int
             Number of thermalized configurations to use in each iteration
        renorm_method : str
             Method of updating harmonic force constants.
             "refit" uses the fit_method to directly fit for harmonic FC
             "least_squares" uses the Moore-Penrose pseudoinverse of A2 to update harmonic FC
        conv_thresh : float
             Desired convergence threshold for free energy per atom in meV
             Default = kB*T/100

        Returns
        -------
        fcp : ForceConstantPotential
             Renormalized at the given temperature
        fcs : ForceConstants
             Renormalized at the given temperature  
        param : np.ndarray
             Renormalized at the given temperature  
        """
        logger.info('################################################')
        logger.info('##### Starting renormalization at {} K #####'.format(str(self.temp)))
        logger.info('################################################')
        conv_thresh = kB*max(10,self.temp)/100 # meV/atom
        logger.info('Convergence threshold: {} meV/atom'.format(conv_thresh))
        natom = self.supercell.get_global_number_of_atoms()
        ndim = natom*3
        nrow = ndim*nconfig
        n2nd = self.cs.get_n_dofs_by_order(2)
        max_order = np.max(self.cs.cutoffs.orders)
        logger.info("Detected maximum order : {}".format(max_order))
        max_even_order = floor(max_order/2)*2
        norder = [self.cs.get_n_dofs_by_order(i) for i in range(2,max_order+1)]

        if max_order < 4:
            ValueError('Maximum order must at least be 4!')
        if np.sum(norder) != len(self.param_BO) :
            ValueError('The length of parameters and the total number of clusters do not match!')
        if self.param_EH is None or self.fcs_EH is None:
            self.param_EH = copy(self.param_BO)
            self.fcs_EH = copy(self.fcs_BO)
            
        # INITIALIZATION 
        param_orig = self.param_EH[:] # initial parameters
        param2orig = param_orig[:norder[0]] # original 2nd order FC parameters
        param2old = copy(param2orig)
        param2renorm_old = np.zeros(len(param2orig)) # renormalized FC2 to be added            
        fcs2 = self.fcs_EH.get_fc_array(2)
        matcov, free_energy_old, imaginary = self.get_qcv(fcs2,initial=True) # initial free energy, print freq
        Lmatcov = self.cholesky_lower(matcov)
        Lmatcov_orig = copy(Lmatcov)
        count = 0

        # Renormalization
        while True :
            count += 1 # iteration counter
            redo = False

            logger.info('# ITERATION {} at {} K'.format(count,self.temp))
            
            # Sample structures and set up fit matrix
            structures = self.qcv_sample(Lmatcov,nconfig,1,False)
            A_mat_EH = np.zeros((nrow,self.cs.n_dofs))
            npar = min(os.cpu_count(),max(1,len(structures)))
            logger.info('{} cores available for parallelization'.format(os.cpu_count()))
            logger.info('{} cores used for parallelization'.format(npar))
            start = psutil.cpu_times().user
            fit_data = Parallel(n_jobs=npar)(
                delayed(construct_fit_data)(self.fcm,structure,s)
                for s,structure in enumerate(structures)
            )
            logger.info('CPU time for fit matrix construction: {} s'.format(psutil.cpu_times().user-start))
            for s,data in enumerate(fit_data):
                A_mat_EH[ndim*s:ndim*(s+1)] = data[0]
                
            if self.renorm_method == "refit":
                # Refits only harmonic FC using forces predicted by FC of all orders  
                f_vec = A_mat_EH.dot(self.param_EH)#.reshape(natom*(s+1),3)
                logger.info('Max. force: {}'.format(np.max(abs(f_vec))))
                opt = Optimizer([A_mat_EH,f_vec],self.fit_method,[0,norder[0]])
                logger.info('Re-fitting effective harmonic IFC...')
                opt.train()
                logger.info(opt)
                self.param_EH[0:n2nd] = opt.parameters # renormalized harmonic
                
            elif self.renorm_method == "least_squares":
                # Renormalizes harmonic FC with anharmonic force components
                A2 = A_mat_EH[:,:n2nd]
                cut3 = np.sum([norder[i] for i in range(1)])
                cut4 = np.sum([norder[i] for i in range(2)])
                A_anh = A_mat_EH[:,cut4:]  # EDIT INDEX for starting order                 
                F_anh = A_anh.dot(self.param_EH[cut4:]) # EDIT INDEX for starting order                    
#                if max_order==4:
#                    A_anh4 = A_mat_EH[:,cut4:]  # EDIT INDEX for starting order
#                    F_anh = A_anh4.dot(self.param_EH[cut4:]) # EDIT INDEX for starting order  
#                elif max_order==6:
#                    cut5 = np.sum([norder[i] for i in range(3)])
#                    cut6 = np.sum([norder[i] for i in range(4)])
#                    A_anh4 = A_mat_EH[:,cut4:cut5]  
#                    F_anh4 = A_anh4.dot(self.param_EH[cut4:cut5])
#                    A_anh6 = A_mat_EH[:,cut6:]
#                    F_anh6 = A_anh6.dot(self.param_EH[cut6:])
#                    F_anh = F_anh4 + F_anh6
                logger.info('Max. anhamonic force: {}'.format(np.max(abs(F_anh))))
                A2inv = np.linalg.pinv(A2) # Moore-Penrose pseudoinverse    
                param2renorm = A2inv.dot(F_anh) # anharmonic renormalizer
                if count == 1:
                    self.param_EH[:n2nd] = self.param_BO[:n2nd] + param2renorm
                else:
                    self.param_EH[:n2nd] = self.param_BO[:n2nd] + 0.5*(param2renorm+param2renorm_old)
                param2renorm_old = copy(param2renorm)

            self.param_EH = enforce_rotational_sum_rules(self.cs, self.param_EH, ["Huang", "Born-Huang"])
            self.fcp_EH = ForceConstantPotential(self.cs, self.param_EH)
            self.fcs_EH = self.fcp_EH.get_force_constants(self.supercell)
            fcs2_EH = self.fcs_EH.get_fc_array(2) # get harmonic FC array
            matcov, free_energy, imaginary = self.get_qcv(fcs2_EH,initial=False)
            Lmatcov = self.cholesky_lower(matcov)
            free_energy_diff = free_energy - free_energy_old
            if free_energy_diff > 5000:
                logger.info('Free energy difference too large!: {} meV/atom'.format(free_energy_diff))
                logger.info('Restarting renormalization...')
                redo = True
                Lmatcov = copy(Lmatcov_orig)                    
            if redo:
                continue

            # Check changes and save to old
            similarity = self._cosine_sim(self.param_EH[0:n2nd],param2old)
            logger.info('Cosine similarity to the previous param2 is {}'.format(str(similarity)))
            logger.info('Change in EH free energy is {} meV/atom'.format(str(free_energy_diff)))
            free_energy_old = free_energy
            param2old = copy(self.param_EH[0:n2nd])
            if np.mod(count,10)==0: # increase threshold if it isn't converging
                conv_thresh *= 2
            # BREAK if relative change in Free Energy is small and similarity is large
            # Write FC in Phonopy/ShengBTE & Hierarchical formats 
            if abs(free_energy_diff) < abs(conv_thresh) and similarity > 0.9 and count > 1 :
                self.fcp_EH.write('solution_{}K.fcp'.format(self.temp))
                np.savetxt('parameters_{}K'.format(self.temp),self.param_EH)
                logger.info('### Renormalization CONVERGED for {} K !!! ###'.format(str(self.temp)))
                break
            
        return self.fcp_EH, self.fcs_EH, self.param_EH

    

    def _cosine_sim(self, a, b):
        """ Calculates cosine similarity between vectors a and b """
        return np.dot(a,b)/np.linalg.norm(a)/np.linalg.norm(b)
    
        
    def _calc_dfree_dPhi2(self,param,param2,fcs2_old,free_energy_old):        
        """ Calculates dfree_dPhi2 (numerical partial derivatives) """
        dparam = 0.0001
        dfree_dfcs2 = np.zeros(param2.shape)
        for i in range(len(param2)):
            logger.info("Partial derivative {} out of {}".format(str(i),str(len(param2))))
            param2diff = np.zeros(len(param2))
            param2diff[i] = param2diff[i] + dparam
            param[0:len(param2)] = param2 + param2diff
            fcp = ForceConstantPotential(self.cs, param)
            fcs = fcp.get_force_constants(self.supercell)
#            fcs.assert_acoustic_sum_rules()
            fcs2 = fcs.get_fc_array(2) # get trial harmonic FC array
            matcov, free_energy, imaginary = self.get_qcv(fcs2) # trial free energy, don't print freq
            Lmatcov = self.cholesky_lower(matcov)
            free_energy_diff = free_energy - free_energy_old
            dfree_dfcs2[i] = free_energy_diff/dparam
        return dfree_dfcs2

    
    def _diagonalize(self, ifc, mass_factor):
        """ Diagonalizes harmonic force constants at Gamma """
        natom = ifc.shape[0]
        # construct the dynamical matrix at Gamma
        dyn = ifc.transpose((0, 2, 1, 3)).reshape((3 * natom, 3 * natom))
        # make copy so we don't overwrite force constants
        dyn = dyn.copy()
        dyn /= mass_factor
        dyn = (dyn + dyn.T)/2 # make symmetric
#        dsyev = scipy.linalg.get_lapack_funcs('syev')
#        eigval, eigvec, info = dsyev(dyn,lower=0)
#        eigval, eigvec, info = scipy.linalg.lapack.dsyev(dyn)#,compute_v=1,lower=0,lwork=3*ndim,overwrite_a=0) 
#        eigval, eigvec = np.linalg.eig(dyn)
#        eigval, eigvec = scipy.linalg.eig(dyn)   
        try:
            eigval, eigvec = scipy.linalg.eigh(dyn,lower=False)
        except:
             eigval, eigvec = np.linalg.eigh(dyn,UPLO='U')  
        return eigval, eigvec
    

    def get_qcv(self,ifc,initial=False):
        """ Calculates quantum covariance matrix

        Parameters
        ----------
        ifc : ForceConstantPotential
            The fcp object of the current step
        ifc_old : ForceConstantPotential
            The fcp object from the previous step
        initial: bool
            If initial setup, then get QCV for 0K
        
        Returns
        -------
        matcov: numpy.ndarray
            quantum covariance matrix (QCV) 
        feng: float
            harmonic free energy (Helmholtz) in meV/atom 
        imaginary: bool
            whether imaginary modes exist
        """
        mass = self.supercell.get_masses()
        mass_factor = np.sqrt(np.outer(np.repeat(mass, 3), np.repeat(mass, 3)))
        natom = ifc.shape[0]

        # Mix force constants and diagonalize
        starttime = time.time()
        eig, evc = self._diagonalize(ifc, mass_factor)
        freq = np.sign(eig)*np.sqrt(np.sign(eig)*eig)*unit2mev
#        if mix=='eigen':
#            eig_old, evc_old = self._diagonalize(ifc_old, mass_factor)
#            freq_old = np.sign(eig_old)*np.sqrt(np.sign(eig_old)*eig_old)*unit2mev
#            freq_old = abs(freq_old)
#            freq = freq*(1-mix_ratio) + freq_old*mix_ratio
#            evc = evc*(1-mix_ratio) + evc_old*mix_ratio
        logger.info(np.sort(freq)[:12]) # print lowest 12 freqs in meV
        imaginary = any(freq<-1e-2)
        freq[np.where(freq<1e-2)] = self.set_im_to*unit2mev # assign small positive to negative (imaginary) values
#        freq = abs(freq)  
        # harmonic free energy and entropy in meV and meV/K
        non_zero = freq > 1e-2
        feng = 0.5*freq
        svib = np.zeros(3*natom)
        nphonon = 1.0
        if initial: # dampen temp to prevent superlarge displacements
            ratio = freq/(kB*self.temp+1) #freq/(kB*min(self.temp+1,50)) # default to 50 K ~ 5 meV
        else:
            ratio = freq/(kB*(self.temp+1)) 
        feng += kB*self.temp*np.log(1.0 - np.exp(-ratio))
        svib += -kB*np.log(1.0 - np.exp(-ratio)) + freq/(self.temp+1)/(np.exp(ratio) - 1.0)
        nphonon += 2.0/(np.exp(ratio) - 1.0)
        feng = feng[non_zero].sum()/natom
        svib = svib[non_zero].sum()/natom
        # quantum covariance
        factors = nphonon/freq
        factors[~non_zero] = 1e-10
        matcov = np.matmul(np.matmul(evc,np.diag(factors)),evc.T)
        matcov *= 0.5*26.2631/(2*np.pi)/mass_factor

        return matcov, feng, imaginary


    def cholesky_lower(self,matcov):
        # Get lower matrix via Cholesky decomposition
        try: # Cholesky could faail if not positive definite
            Lmatcov = scipy.linalg.cholesky(matcov,lower=True)
        except: # Cholesky with added multiple of identity (Nocedal & Wright, p.51)
            if np.min(matcov) > 0 :
                tau = 0
            else:
                beta = 0.1
                tau = -np.min(matcov)+beta
            while True:
                matcov = matcov+tau*np.eye(matcov.shape[0])
                try:
                    Lmatcov = scipy.linalg.cholesky(matcov,lower=True)
                    break
                except:
                    tau = max(2*tau,beta)
        return Lmatcov
    
    
    def qcv_sample(self,Lmatcov,nconfig,purpose,use_seed):
        """ Create displacements using quantum covariance

        Parameters
        ----------
        Lmatcov: numpy.ndarray
              Cholesky-decomposed lower quantum covariance matrix (QCV)
        nconfig: int
              Number of configurations to generate
        purpose: int
              zero - return atom-displacements structures to be wrtten for thermalization
              nonzero - returns structures with set_array(displacements) for renormalization
        use_seed: bool
              If True, uses random seed

        Returns
        -------
        structures : StructureContainer
              Contains supercell configurations with atom-displacement according to QCV
        """
        pos = self.supercell.get_scaled_positions()
        ndim = Lmatcov.shape[0]
        mu = 0.0
        sigma = 1.0 
        if use_seed:
            random.seed(1)
        dispAll = [random.normal(mu, sigma, ndim) for iconf in range(nconfig)] # Gaussian random sampling
        structures = [ self._dispWrap(dispAll,Lmatcov,iconf,purpose) for iconf in range(nconfig) ]
        logger.info('{} qcv-displaced structures'.format(str(len(structures))))
        return structures


    def _dispWrap(self,dispAll,Lmatcov,iconf,purpose):
        """ Applies displacements to supercells """
        natom = self.supercell.get_global_number_of_atoms()
        latvec = np.array(self.supercell.get_cell())
        pos = self.supercell.get_positions()
        disp = (np.dot(Lmatcov, dispAll[iconf])).reshape((natom, 3))
        logger.info('Max, Avg displacement: {}, {}'.format(abs(disp).max(),abs(disp).mean()))
        pos = pos + disp
        supercell_tmp = self.supercell.copy()
        supercell_tmp.set_positions(pos)
        displacements = get_displacements(supercell_tmp, self.supercell)
        supercell_disp = self.supercell.copy()
        supercell_disp.set_array('displacements', displacements)
        supercell_disp.set_array('forces', np.zeros((len(displacements),3)))
        if purpose == 0: # create/write thermalized cells
            return supercell_tmp
        else: # for renormalization
            return supercell_disp

        
    def reconstruct_anharmonic_qcv(self,structures):
        natom = self.supercell.get_global_number_of_atoms()
        ndim = 3*natom
        nrow = ndim*len(structures)
        n2nd = self.cs.get_n_dofs_by_order(2)
        ncut = n2nd + self.cs.get_n_dofs_by_order(3)
        A_mat = np.zeros((nrow,self.cs.n_dofs))
        npar = min(os.cpu_count(),max(1,len(structures)))
        logger.info('{} cores available for parallelization'.format(os.cpu_count()))
        logger.info('{} cores used for parallelization'.format(npar))
        start = psutil.cpu_times().user
        results = Parallel(n_jobs=npar)(
            delayed(construct_fit_data)(self.fcm,structure,s)
            for s,structure in enumerate(structures)
        )
        logger.info('CPU time for fit matrix construction: {} s'.format(psutil.cpu_times().user-start))
        for i,result in enumerate(results):
            A_mat[i*ndim:(i+1)*ndim] = result[0]
        A2inv = np.linalg.pinv(A_mat[:,:n2nd]) # Moore-Penrose pseudoinverse
        param_anh = self.param_BO.copy()
        param2anh = np.matmul(A2inv,np.matmul(A_mat[:,ncut:],self.param_BO[ncut:])) # harmonic approx of anharmonic effect   
        param_anh[:n2nd] = param2anh # replace the harmonic part with the anharmonic approx.
        fcp_anh = ForceConstantPotential(self.cs, param_anh)
        fcs_anh = fcp_anh.get_force_constants(self.supercell)
        matcov_anh, _, imaginary = self.get_qcv(fcs_anh.get_fc_array(2))
        return matcov_anh

    def born_oppenheimer_qcv(self,nconfig):
        logger.info('Reconstructing Born-Oppenheimer QCV at {}K...'.format(self.temp))
        natom = self.supercell.get_global_number_of_atoms()
        ndim = 3*natom
        AR_nconfig = nconfig*(1+int(self.temp/20)) # number of configs to use for anharmonic reconstruction
        logger.info("   Calculating bare harmonic QCV...")
        matcov_h, _, imaginary_h = self.get_qcv(self.fcs_BO.get_fc_array(2))
        Lmatcov_h = self.cholesky_lower(matcov_h)
        structures_h = self.qcv_sample(Lmatcov_h,AR_nconfig,1,False)
        logger.info("   Calculating bare anharmonic QCV...")
        matcov_anh = self.reconstruct_anharmonic_qcv(structures_h)
#        matcov_anh = self.anharmonic_qcv()
        Lmatcov_anh = self.cholesky_lower(matcov_anh)
        structures_anh = self.qcv_sample(Lmatcov_anh,AR_nconfig,1,False)
        logger.info("   Calculating harmonic-anharmonic cross QCV...")
        assert(len(structures_h)==len(structures_anh))
        disp_h = np.zeros((len(structures_h),ndim))
        disp_anh = np.zeros((len(structures_anh),ndim))
        matcov_hanh = np.zeros((ndim,ndim))
        for i,(s_h,s_anh) in enumerate(zip(structures_h,structures_anh)):
            disp_h[i] = s_h.get_array('displacements').ravel()
            disp_anh[i] = s_anh.get_array('displacements').ravel()
        dev_h = disp_h - disp_h.mean(axis=0)
        dev_anh = disp_anh - disp_anh.mean(axis=0)
        for i in range(len(structures_h)):
            matcov_hanh += np.outer(dev_h,dev_anh[i]).reshape(len(structures_h),ndim,ndim).sum(axis=0)
        matcov_hanh /= len(structures_h)**2
        logger.info("   Calculating BO QCV...")
        matcov_BO = 0.5**2*(matcov_h + matcov_anh + 2*matcov_hanh)
        Lmatcov_BO = self.cholesky_lower(matcov_BO)
        structures_BO = self.qcv_sample(Lmatcov_BO,AR_nconfig,1,False)
        logger.info("   Calculating EH harmonic QCV...")
        matcov_EH, _, imaginary_EH = self.get_qcv(self.fcs_EH.get_fc_array(2))
        Lmatcov_EH = self.cholesky_lower(matcov_EH)
        structures_EH = self.qcv_sample(Lmatcov_EH,AR_nconfig,1,False)
        logger.info("   Calculating EH-BO cross QCV...")
        assert(len(structures_EH)==len(structures_BO))
        disp_EH = np.zeros((len(structures_EH),ndim))
        disp_BO = np.zeros((len(structures_BO),ndim))
        matcov_EHBO = np.zeros((ndim,ndim))
        for i,(s_EH,s_BO) in enumerate(zip(structures_EH,structures_BO)):
            disp_EH[i] = s_EH.get_array('displacements').ravel()
            disp_BO[i] = s_BO.get_array('displacements').ravel()
        dev_EH = disp_EH - disp_EH.mean(axis=0)
        dev_BO = disp_BO - disp_BO.mean(axis=0)
        for i in range(len(structures_EH)):
            matcov_EHBO += np.outer(dev_EH,dev_BO[i]).reshape(len(structures_EH),ndim,ndim).sum(axis=0)
        matcov_EHBO /= len(structures_EH)**2
        return matcov_EH, matcov_BO, matcov_EHBO


    def thermodynamic_integration(self,lambda_array,matcov_EH,matcov_BO,matcov_EHBO,nconfig):
        d_U_d_lambda = np.zeros(len(lambda_array))
        standard_error = np.zeros(len(lambda_array))
        logger.info('Thermodynamic integration at {}K...'.format(self.temp))
        for l,lamb in enumerate(lambda_array):
            logger.info('Lambda = {}'.format(lamb))
            TI_nconfig_l = int(nconfig*(1+5*lamb)*(1+self.temp/100))

            # Covariance-mixing
            matcov_lambda = (1-lamb)**2*matcov_EH + lamb**2*matcov_BO + 2*(1-lamb)*lamb*matcov_EHBO
            Lmatcov_lambda = self.cholesky_lower(matcov_lambda)
            logger.info("   Lambda-dependent QCV constructed")
                
            # sample structures and calculate energy
            structures = self.qcv_sample(Lmatcov_lambda,TI_nconfig_l,1,False)
            U_EH = np.zeros(TI_nconfig_l)
            U_BO = np.zeros(TI_nconfig_l)
            npar = min(os.cpu_count(),max(1,len(structures)))
            logger.info('{} cores available for parallelization'.format(os.cpu_count()))
            logger.info('{} cores used for parallelization'.format(npar))
            start = psutil.cpu_times().user
            U_results = Parallel(n_jobs=npar)(
                delayed(self._calculate_U)(structure.get_array('displacements'),s)
                for s,structure in enumerate(structures)
            )
            logger.info('     CPU time for potential energy calculation: {} s'.format(psutil.cpu_times().user-start))
            for s,U_result in enumerate(U_results):
                U_EH[s] = U_result[0]
                U_BO[s] = U_result[1]
            logger.info('     Lambda={} <dU/dL>: {} eV/atom'.format(lamb,(U_BO-U_EH).mean()))
            d_U_d_lambda[l] = (U_BO - U_EH).mean()
            standard_error[l] = np.std(U_BO - U_EH)/np.sqrt(TI_nconfig_l)

        # is a harmonic material and only lambda=0 is included
        if len(lambda_array)==1 and lambda_array[0]==0:
            correction_TI = d_U_d_lambda[0]
        else: # is dynamically unstable material and full lambda integration is done
            filt = np.array([((d_U_d_lambda[i]-d_U_d_lambda[i-1])/(lambda_array[i]-lambda_array[i-1]) < ((self.temp+10)//10)*1e-2) & # eV/lambda
                             ((d_U_d_lambda[i]-d_U_d_lambda[i-1])/(lambda_array[i]-lambda_array[i-1]) > -((self.temp+10)//10)*1e-1) # eV/lambda
                             for i in range(1,len(d_U_d_lambda))]) # outliar filter for unphysically abrupt increase or decrease
            filt = np.append(True,filt)
            lambda_final = lambda_array[filt]
            d_U_d_lambda_final = d_U_d_lambda[filt]
            standard_error_final = standard_error[filt]
            if lambda_final[-1] != 1.0: # ensure lambda=1.0 is included
                lambda_final = np.append(lambda_final,1.0)
                d_U_d_lambda_final = np.append(d_U_d_lambda_final,d_U_d_lambda_final[-1])
                standard_error_final = np.append(standard_error_final,0.0)
            logger.info('Final lambda: {}'.format(lambda_final))
            logger.info('Final <dUdl>: {} eV/atom'.format(d_U_d_lambda_final))
            logger.info('Final std(<dUdl>): {} eV/atom'.format(d_U_d_lambda_final))
            correction_TI = np.trapz(d_U_d_lambda_final,lambda_final)
        logger.info('Free energy correction via thermodynamic integration: {}'.format(correction_TI))
        return correction_TI
        

    def _calculate_U(self,displacements,s):
        A = self.fcm.get_fit_matrix(displacements)
        n2nd = self.cs.get_n_dofs_by_order(2)
        natom = displacements.shape[0]
        param_EH_scale = self.param_EH.copy()
        param_BO_scale = self.param_BO.copy()
        # "Forces" with Taylor factorial coefficients adjusted for correct energy calculation
        ind_prev = 0
        for order in self.cs.cutoffs.orders:
            ind = self.cs.get_n_dofs_by_order(order) + ind_prev
            param_EH_scale[ind_prev:ind] = self.param_EH[ind_prev:ind]/order
            param_BO_scale[ind_prev:ind] = self.param_BO[ind_prev:ind]/order
            ind_prev = ind
        force_EH = np.matmul(A[:,:n2nd],param_EH_scale[:n2nd])
        force_BO = np.matmul(A,param_BO_scale)
        energy_BO = -np.dot(displacements.ravel(),force_BO)/natom
        energy_EH = -np.dot(displacements.ravel(),force_EH)/natom
        logger.info('    {} U_BO, U_EH, diff : {}, {}, {} eV/atom'.format(s,energy_BO,energy_EH,energy_BO-energy_EH))
        return [energy_EH,energy_BO]


    def _add_default_metadata(self):
        """Adds default metadata to metadata dict."""
        import getpass
        import socket
        from datetime import datetime
        from . import __version__ as hiphive_version

        self._metadata['date_created'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        self._metadata['username'] = getpass.getuser()
        self._metadata['hostname'] = socket.gethostname()
        self._metadata['hiphive_version'] = hiphive_version
