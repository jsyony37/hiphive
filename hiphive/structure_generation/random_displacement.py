"""
Module for generating displacement of atoms by a specified distance
The direction of displacement (x,y,z) is randomly selected
"""

import numpy as np


def generate_displaced_structures(atoms, n_structures, distance):
    """Returns list of rattled configurations.

    Displacements are drawn from normal distributions for each
    Cartesian directions for each atom independently.

    Warning
    -------
    Repeatedly calling this function *without* providing different
    seeds will yield identical or correlated results. To avoid this
    behavior it is recommended to specify a different seed for each
    call to this function.

    Parameters
    ----------
    atoms : ase.Atoms
        prototype structure
    n_structures : int
        number of structures to generate
    distance : float
        displacement distance

    Returns
    -------
    list of ase.Atoms
        generated structures
    """
    N = len(atoms)
    atoms_list = []
    for _ in range(n_structures):
        atoms_tmp = atoms.copy()
        # random choice of direction
        directions = np.random.normal(size=(N, 3))
        normalizer = np.linalg.norm(directions, axis=1, keepdims=True)
        displacements = distance*directions/normalizer
        atoms_tmp.positions += displacements
        atoms_list.append(atoms_tmp)
    return atoms_list
