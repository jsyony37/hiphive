# Base image
FROM continuumio/miniconda3

# Base packages
# git is used in hiphive-testing and included here to avoid two near identical images
RUN \
  apt-get update -qy && \
  apt-get upgrade -qy && \
  apt-get install -qy \
    git \
    graphviz \
    pandoc \
    python3-pip \
    zip

RUN \
  pip3 install --upgrade \
    pip \
  && \
  pip3 install --upgrade \
    coverage \
    flake8 \
    nbmake \
    pytest \
    setuptools_scm \
    twine \
    xdoctest

# Packages needed for hiphive
RUN \
  pip3 install \
    h5py \
    numba>=0.55 \
    numpy>=1.18 \
    sympy>=1.1 \
  && \
  pip3 install \
    ase \
    pandas \
    phonopy \
    scikit-learn \
    scipy>=1.0.0 \
    spglib

# Packages for building documentation
RUN \
  pip3 install \
    sphinx_autodoc_typehints \
    sphinx-rtd-theme \
    sphinx_sitemap \
    sphinxcontrib-bibtex \
    nbsphinx

RUN \
  conda install -c conda-forge phono3py

CMD /bin/bash
