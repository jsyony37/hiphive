# Compiling the documentation

The documentation (the user guide) is automatically build and deployed during
the CI cycle on gitlab.  If for some reason one must compile a local version
(for example while expanding or updating the documentation) it can be compiled
as follows::

    pip3 install --user sphinx
    pip3 install --user sphinx_rtd_theme
    pip3 install --user sphinx_sitemap
    pip3 install --user cloud_sptheme

    cd hiphive
    sphinx-build doc/source/ doc_build
    firefox doc_build/index.html
