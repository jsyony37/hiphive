.. _renormalization:
.. highlight:: python
.. index::
   single: Anharmonic phonon renormalization

.. |br| raw:: html

  <br/>


Temperature-dependent anharmonic phonon renormalization
=======================================================

This scheme offers a way to treat temperature-dependent phonon dispersion
through high-order anharmonic force constants (currently up to 4th order).
Anharmonic effects directly renormalize harmonic force constants at finite
tmperaturees, which is capable of stabilizng otherwise dynamically unstable
phonons at finite temperatures.

The efficiency of the present method lies in 1) usage of quantum covariance
to generate temperature-dependent atomic displacements, rather than cumbersome
AIMD simulations at each temperature, 2) DFT force calculations required for
only a handful supercells, and 3) rapid extraction of high-order anharmonic
force constants via hiPhive.

The entirity of anharmonic phonon renormalization is performed within the
:class:`Renormalization <hiphive.anharmonic.Renormalization>` class. It can be
imported and an object can be created as follows::

  from hiphive.anharmonic import Renormalization
  renorm = Renormalization(cs, super_Atoms, fcs, param, T)

The required inputs are a :class:`ClusterSpace <hiphive.ClusterSpace>` object,
an :class:`Atoms <ase.Atoms>` object for the unperturbed supercell, the
:class:`ForceConstants <hiphive.ForceConstants>` object and the parameter array
from force-fitting, and finally the desired temperature at which renormalization
will take place.

<<<<<<< HEAD
From there one, one can either generate thermalized (atoms-displaced at the given
temperature) supercell configurations for training via::
=======
From there one, one can either generate and write thermalized (atoms-displaced
at the given temperature) supercell configurations for training via::
>>>>>>> personal

  renorm.thermalize_training_set(nconfig,path)

or initiate the renormalization routine via::
  
  fcp, fcs, param = renorm.renormalize(nconfig,renorm_method,conv_thresh)

whereby the number of configurations and free energy converegence threshold are
to be defined as inputs. Renormalization results in new sets of force constants.
<<<<<<< HEAD
=======

.. container:: toggle

    .. container:: header

       A minimal example can be found in |br|
       ``tests/integration/fcs_sensing.py``

    .. literalinclude:: ../../../tests/integration/fcs_sensing.py
>>>>>>> personal
