import unittest
import tempfile
import numpy as np
import sys

from io import StringIO
from itertools import product, combinations_with_replacement
from ase.build import bulk
from hiphive import ClusterSpace, ForceConstantPotential, ForceConstants
from hiphive.force_constants import SortedForceConstants, RawForceConstants
from hiphive.force_constants import symbolize_force_constant,\
    array_to_dense_dict, check_label_symmetries, dense_dict_to_sparse_dict

from hiphive import StructureContainer, ForceConstantPotential, ClusterSpace, enforce_rotational_sum_rules
from hiphive.utilities import get_displacements
from hiphive.renormalization import Renormalization

def generate_random_force_constant(cluster, symmetric):
    """ Helper function for generating random force constant tensors. """
    shape = (3, ) * len(cluster)
    if symmetric:
        from hiphive.core.eigentensors import init_ets_from_label_symmetry
        fc = np.zeros(shape)
        for et in init_ets_from_label_symmetry(cluster):
            fc += et * np.random.random()
    else:
        fc = np.random.random((shape))
    return fc


class TestRenormalization(unittest.TestCase):
    """
    Unittest class for Renormalization.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # setup ClusterSpace
        cutoffs = [5.0,4.0,3.0]
        prim = bulk('Al', 'fcc', a=4.0)
        self.cs = ClusterSpace(prim, cutoffs)
        # random temperature and fit method
        self.temp = 300
        self.fit_method = 'rfe'
        # random force constant parameters
        self.param = np.random.rand(self.cs.n_dofs)
        self.fcp = ForceConstantPotential(self.cs, self.param)

        # supercell
        self.supercell = bulk('Al', 'fcc', a=4.0).repeat((5, 5, 5))
        self.n_atoms = len(self.supercell)
        self.fcs = self.fcp.get_force_constants(self.supercell)
        
<<<<<<< HEAD
        self.renorm = Renormalization(self.cs, self.fcs, self.param, self.temp, self.fit_method)
=======
        self.renorm = Renormalization(self.cs, self.supercell, self.fcs, self.param, self.temp, self.fit_method)
>>>>>>> personal

    def shortDescription(self):
        """ Prevents unittest from printing docstring in test cases. """
        return None

    def test_init(self):
        """ Test initializing Renormalization. """

        test = Renormalization(self.cs, self.supercell, self.fcs, self.param, self.temp, self.fit_method)
        self.assertIsInstance(test, Renormalization)

        # initialize with param longer than n_dof
        test_param = np.random.rand(1+self.cs.n_dofs)
        with self.assertRaises(ValueError):
            Renormalization(self.cs, self.supercell, self.fcs, test_param, self.temp, self.fit_method)

        # initialize with supercell of a different size
        test_supercell = bulk('Al', 'fcc', a=4.0).repeat((4, 4, 4))
        with self.assertRaises(ValueError):
            Renormalization(self.cs, test_supercell, self.fcs, self.param, self.temp, self.fit_method)

        # initialize with fcs on a different supercell
        test_fcs = self.fcp.get_force_constants(test_supercell)
        with self.assertRaises(ValueError):
            Renormalization(self.cs, self.supercell, test_fcs, self.param, self.temp, self.fit_method)
            
    def test_thermalize_training_set(self):
        """ Test method thermalize_training_set """
        nconfig = 2 # random number of configurations
        structures = self.renorm.thermalize_training_set(nconfig,True)
        assert len(structures) == nconfig
        for i, structure in enumerate(structures):
            assert (structure.get_atomic_numbers() == self.supercell.get_atomic_numbers()).all()
            assert (np.array(structure.get_cell()) == np.array(self.supercell.get_cell())).all()
            assert (structure.get_positions() != self.supercell.get_positions()).all()
        
    def test_renormalize(self):
        """ Test method renormalize """
        nconfig = 1 # random number of configurations
<<<<<<< HEAD
        renorm_method = 'full'
=======
        renorm_method = 'pseudoinverse'
>>>>>>> personal
        conv_thresh = 100 # some large number
        fcp, fcs, param = self.renorm.renormalize(nconfig,renorm_method,conv_thresh)
        self.assertIsInstance(fcp, ForceConstantPotential)
        self.assertIsInstance(fcs, ForceConstants)
        assert(len(param)==len(self.param))

    def test_get_qcv_and_qcv_displace(self):
        """ Test methods get_qcv and qcv_displace """
        nconfig = 2
        ifc = self.fcs.get_fc_array(2)
        feng, Lmatcov = self.renorm.get_qcv(ifc,ifc)
        assert Lmatcov.shape == (self.n_atoms*3,self.n_atoms*3)
        
        for purpose in [0,1]:
            structures = self.renorm.qcv_displace(Lmatcov,nconfig,purpose,True)
            assert len(structures) == nconfig
            for i, structure in enumerate(structures):
                assert (structure.get_atomic_numbers() == self.supercell.get_atomic_numbers()).all
                assert (structure.get_cell() ==self.supercell.get_cell()).all
                if purpose == 0:
                    assert (structure.get_positions() != self.supercell.get_positions()).all()
                elif purpose == 1:
                    assert (structure.get_positions() == self.supercell.get_positions()).all()
